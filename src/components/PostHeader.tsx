import React, { useEffect, useState, useRef, useCallback } from "react";

import styles from "./PostHeader.module.scss";
import { useNavigate } from "react-router-dom";

const PostHeader: React.FunctionComponent<{ text: string }> = ({ text }) => {
  const headerRef = useRef(null);
  const navigate = useNavigate();
  const [prevScrollTop, setScrollTop] = useState(window.pageYOffset);

  const scrollListener = useCallback(() => {
    const yOffset = window.pageYOffset;

    if (headerRef.current && headerRef.current !== null) {
      const header = headerRef.current as unknown as HTMLDivElement;

      const headerHeight = header.clientHeight;

      if (yOffset > headerHeight) {
        header.style.transform = `translateY(-${headerHeight}px)`;
        header.style.position = "fixed";

        setTimeout(() => {
          header.style.transition = "all .3s";
        }, 100);
      }
    }

    if (window.pageYOffset < prevScrollTop) {
      if (headerRef.current && headerRef.current !== null) {
        const header = headerRef.current as unknown as HTMLDivElement;
        header.style.transition = "all .1s";
        header.style.transform = `translateY(0)`;

        if (yOffset === 0) {
          header.style.position = "absolute";
          header.style.transition = "none";
        }
      }
    }

    setScrollTop(yOffset);
  }, [prevScrollTop]);

  useEffect(() => {
    window.addEventListener("scroll", scrollListener, false);
    return () => {
      window.removeEventListener("scroll", scrollListener, false);
    };
  }, [scrollListener]);

  return (
    <header className={styles.postHeader} ref={headerRef}>
      <i
        className="las la-angle-left"
        onClick={() => {
          navigate(-1);
        }}
      />
      <p>
        Published in <br /> <span>{text}</span>
      </p>
    </header>
  );
};

export default PostHeader;
