import React, { useState, MouseEvent, Dispatch, SetStateAction } from "react";
import { CSSTransition } from "react-transition-group";
import Filter from "./Filter";
import AddPost from "./AddPost";

const LoggedInNav: React.FunctionComponent<{
  setFilter: Dispatch<SetStateAction<boolean>>;
}> = ({ setFilter }) => {
  return <></>;
};

export default LoggedInNav;
