import React from "react";
import { useNavigate } from "react-router-dom";

const AddPost: React.FunctionComponent = () => {
  const navigate = useNavigate();

  const handleButton = () => {
    navigate("/create");
  };

  return (
    <div className="addPostButton btn-round" onClick={handleButton}>
      <i className="las la-file-alt"></i>
    </div>
  );
};

export default AddPost;
