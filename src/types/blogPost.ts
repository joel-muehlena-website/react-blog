export interface BlogPost {
  _id: string;
  title: string;
  image?: string;
  description: string;
  authorName: string;
  authorId: string;
  body: string;
  category: string;
  tags: Array<string>;
  createdAt: Date;
  updatedAt: Date;
}
