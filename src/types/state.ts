import { BlogPost } from "./blogPost";
import { User, Author } from "./user";

export interface ReduxState {
  blogPosts: {
    isLoading: boolean;
    allPosts: Array<BlogPost>;
    error: any;
    createSucceed: boolean;
    updateSucceed: boolean;
    deleteSucceed: boolean;
    createNewPostError: {
      state: boolean;
      data: string;
    };
    updatePostError: {
      error: boolean;
      data: string;
    };
    deletePostError: {
      error: boolean;
      data: string;
    };
  };
  blogAuthors: {
    isLoading: boolean;
    allAuthors: Array<Author>;
    author: Author;
    error: any;
  };
  user: {
    isAuthenticated: boolean;
    isLoading: boolean;
    loading: boolean;
    data: User;
  };
  filterOptions: {
    loading: boolean;
    error: string;
    data: {
      authors: Array<string>;
      tags: Array<string>;
      categories: Array<string>;
    };
  };
}
