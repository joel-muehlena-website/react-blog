export interface RoleVerificationProps {
  isVerified: boolean;
}
