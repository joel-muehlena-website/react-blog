export interface NewPostPayload {
  author: string;
  title: string;
  description: string;
  id: string;
  body: string;
  tags: string;
  category: string;
}

export interface UpdatePostPayload {
  postId: string;
  title: string;
  description: string;
  body: string;
  tags: string;
  category: string;
}
