export interface User {
  id: string;
  roles: string;
  firstName: string;
  lastName: string;
  username: string;
  avatar: string;
}

export interface Author {
  _id: string;
  userId: string;
  role: string;
  bio: string;
  firstName: string;
  lastName: string;
  username: string;
  avatar: string;
}
