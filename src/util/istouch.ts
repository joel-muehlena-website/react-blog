export const isTouchDevice = (): boolean => {
  return (
    "ontouchstart" in (window as any) ||
    (navigator as any).maxTouchPoints > 0 ||
    (navigator as any).msMaxTouchPoints > 0
  );
};
