FROM node:11.15-alpine

ENV NODE_ENV="production"

WORKDIR /app

COPY server.js .

RUN npm i express

RUN mkdir build

COPY ./build ./build

EXPOSE 80

CMD ["node", "server"]